﻿using Microsoft.AspNetCore.Mvc;
using PgSqlCodeFirst.Models;
using PgSqlCodeFirst.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PgSqlCodeFirst.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TestController : Controller
    {
        private readonly ITestService _testService;

        public TestController(ITestService testService)
        {
            _testService = testService;
        }

        [HttpGet]
        public IEnumerable<Test> Get()
        {
            return _testService.GetTests();
        }

        [HttpPost]
        public Task Create()
        {
            return _testService.CreateTestAsync(new Test { Prop1 = DateTime.UtcNow.ToString("MM/dd/yyyy hh:mm:ss.fff tt"), Prop2= "test"});
        }
    }
}
