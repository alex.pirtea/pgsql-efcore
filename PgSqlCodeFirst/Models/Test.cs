﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PgSqlCodeFirst.Models
{
    public class Test
    {
        [Key]
        public string Prop1 { get; set; }
        public string Prop2 { get; set; }
    }
}

