﻿using PgSqlCodeFirst.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PgSqlCodeFirst.Services
{
    public interface ITestService
    {
        public List<Test> GetTests();
        public Task CreateTestAsync(Test test);
    }
}
