﻿using PgSqlCodeFirst.DbContexts;
using PgSqlCodeFirst.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PgSqlCodeFirst.Services
{
    public class TestService : ITestService
    {
        private readonly TestContext _testContext;

        public TestService(TestContext testContext)
        {
            this._testContext = testContext;
        }
        public async Task CreateTestAsync(Test test)
        {
            await _testContext.Tests.AddAsync(test);
            await _testContext.SaveChangesAsync();
        }

        public  List<Test> GetTests()
        {

            return _testContext.Tests.Select(x => x).ToList();
        }
    }
}
