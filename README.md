# pgsql-efcore

To run a local pgsql database with docker run:  
`docker run --rm -p 5432:5432 -e POSTGRESQL_USERNAME=admin -e POSTGRESQL_PASSWORD=password -e POSTGRESQL_DATABASE=test bitnami/postgresql:latest `
